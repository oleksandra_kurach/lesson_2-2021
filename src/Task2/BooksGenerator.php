<?php

declare(strict_types=1);

namespace App\Task2;

class BooksGenerator
{
    public function __construct(
        private int $minPagesNumber,
        private array $libraryBooks,
        private int $maxPrice,
        private array $storeBooks
    )
    {
    }

    public function generate(): \Generator
    {
        foreach ($this->libraryBooks as $book){
            if($book->getPagesNumber() >= $this->minPagesNumber){
                yield $book;
            }
        }

        foreach ($this->storeBooks as $book){
            if ($book->getPrice() <= $this->maxPrice){
                yield $book;
            }
        }

    }
}