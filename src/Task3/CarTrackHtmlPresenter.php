<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\Track;

class CarTrackHtmlPresenter
{
    public function present(Track $track): string
    {
        $cars = $track->all();

        $presentations = array_map(function ($car): string {
            return $this->presentSingleCar($car);
        },$cars);

        return implode("", $presentations);
    }

    public function presentSingleCar($car): string
    {
        $image = $car->getImage();

        $name = $car->getName();

        $carDetails = implode(', ', [
            $car->getSpeed(),
            $car->getPitStopTime(),
            $car->getFuelTankVolume(),
            $car->getFuelConsumption()
        ]);

        return "<img src=\"$image\"> <p>$name: $carDetails</p>";
    }
}