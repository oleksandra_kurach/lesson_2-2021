<?php

declare(strict_types=1);

namespace App\Task1;

class Track
{
    private float $lapLength;
    private int $lapsNumber;
    /**
     * @var Car[] $cars
     */
    private array $cars = [];

    public function __construct(float $lapLength, int $lapsNumber)
    {
        $this->lapLength = $lapLength;
        $this->lapsNumber = $lapsNumber;
    }

    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
        $this->cars[] = $car;
    }

    public function all(): array
    {
        return $this->cars;
    }

    public function run(): Car
    {
        $bestTime = 0;
        $bestCar = null;

        foreach ($this->cars as $car) {
            $distance = $this->getTotalDistance();
            $driveTime = $distance / $car->getSpeed();

            $totalTime = $driveTime + $this->calculateTotalPitStopTime($car, $distance);

            if ($bestTime === 0 || $totalTime < $bestTime){
                $bestTime = $totalTime;
                $bestCar = $car;
            }
        }

        return $bestCar;
    }

    /**
     * @return float
     */
    public function getTotalDistance(): float
    {
        return $this->lapsNumber * $this->lapLength;
    }


    private function calculateTotalPitStopTime(Car $car,float $distance): float
    {
        $fuelNeeded = $distance * $car->getFuelConsumption() / 100;
        $numberOfPitstops = ceil($fuelNeeded / $car->getFuelTankVolume()) - 1;

        return ($numberOfPitstops) * $car->getPitStopTime() / 3600;
    }
}
